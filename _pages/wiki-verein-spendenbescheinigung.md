---
layout: page
title: Spendenbescheinigung
permalink: /wiki/verein/spendenbescheinigung/
---
Für Spenden ab 200,- Euro stellen wir auf Nachfrage gerne eine schriftliche Spendenbescheinigung aus.

Dazu benötigen wir die vollständige Anschrift.

<h3>Vereinfachter Spendennachweis</h3>

Bis 200,- Euro (pro Einzelspende!) reicht dem Finanzamt ein vereinfachter Spendennachweis aus.<br>

Dieser wird zusammen mit den Kontoauszügen eingereicht.

Der Vordruck kann hier heruntergeladen werden: <a href="/wiki/_media/verein/vereinfachter-spendennachweis.pdf" class="media mediafile mf_pdf" title="verein:vereinfachter-spendennachweis.pdf">Spendennachweis</a>


Unterkonto 1126825603: <a href="/wiki/_media/verein/vereinfachter-spendennachweis-tails.pdf" class="media mediafile mf_pdf" title="verein:vereinfachter-spendennachweis-tails.pdf">Spendennachweis</a>


Unterkonto 1126825606: <a href="/wiki/_media/verein/vereinfachter-spendennachweis-riseup.pdf" class="media mediafile mf_pdf" title="verein:vereinfachter-spendennachweis-riseup.pdf">Spendennachweis</a>


<h3>Freistellungsbescheid</h3>

Der aktuelle Freistellungsbescheid vom Finanzamt Dresden-Nord kann hier heruntergeladen werden:  <a href="/wiki/_media/verein/freistellungsbescheid_2017.pdf" class="media mediafile mf_pdf" title="verein:freistellungsbescheid_2017.pdf"> Freistellungsbescheid</a>