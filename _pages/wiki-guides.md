---
layout: page
title: Setup Guides
permalink: /wiki/guides/
---
  - [Exit Relay Setup](/wiki/setup/server)

## Outdated

  - ~~[Tor with libevent2](/wiki/setup/libevent2)~~
  - ~~[Block BitTorrent trackers](/wiki/setup/blockbittorrent)~~
  - ~~[Centrally update MyFamily](/wiki/setup/updatefamily)~~
  - ~~[Setting up a Tor Bridge in less than 5 Minutes](/wiki/setup/bridge)~~
