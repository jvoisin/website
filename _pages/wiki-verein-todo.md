---
layout: page
title: TODO
permalink: /wiki/todo/
---
We're a slowly, but healthily growing family of Tor friends. Anyone is welcome to contribute, priority on the items are basically set by individual preferences (“Hey, I started…”). Join us on irc.oftc.net #torservers.

<ol>
<li class="level1"><div class="li"> Work on Todo-List, add ideas :)</div>
</li>
</ol>

<h2 class="sectionedit2" id="association">Association</h2>
<div class="level2">
<ol>
<li class="level1"><div class="li"> Collect information on Talks and Workshops that we do. </div>
</li>
<li class="level1"><div class="li"> Document information on Talks and Workshops in december as we need it as a yearly proof of work to stay a non-profit.</div>
</li>
</ol>

</div>

<h2 class="sectionedit3" id="tech">Tech</h2>
<div class="level2">

</div>

<h3 class="sectionedit4" id="bridges">Bridges</h3>
<div class="level3">

<p>
→ <a href="/wiki/hoster/bridges" class="wikilink1" title="hoster:bridges" data-wiki-id="hoster:bridges">Tor Bridges Action Request</a>
</p>

</div>

<h3 class="sectionedit5" id="exits">Exits</h3>
<div class="level3">
<ul>
<li class="level1"><div class="li"> monitoring</div>
</li>
<li class="level1"><div class="li"> graphs. I want combined graphs that for example show bandwidth usage across all nodes, and “our bandwidth” in comparison to all exit relay bandwidth (and some usage stats, see bridges)</div>
</li>
<li class="level1"><div class="li"> myFamily setting: currently, we have to log into EACH machine and update MyFamily if we add or remove a node. We need that centralized, but safe (signed). also, we need to be able to sync (some) exit rules. – 04/04/11 I'm working on it (tagnaq)</div>
</li>
</ul>

</div>

<h3 class="sectionedit6" id="tor_distribution">Tor Distribution</h3>
<div class="level3">

<p>
I wrote a simple shell script that generates a torrent per file in Torproject's dist directory, carrying several Tor mirrors as “web seeds”. Bittorrent is a nice way to circumvent filters because it uses a DHT to find other peers.
</p>

<p>
The script should be improved to create useful torrents instead of a single torrent per file. RRansom started working on a Lua script that does a proper selection, but mktorrent only supports to create torrents for either one file or one directory. Look for a different torrent creation tool that supports a selection of files, or create symbolic links to the files, run mktorrent, remove symbolic links.
</p>

<p>
Scripts and torrents can be found at <a href="https://www.torservers.net/mirrors/" class="urlextern" title="https://www.torservers.net/mirrors/" rel="ugc nofollow">https://www.torservers.net/mirrors/</a>
</p>

<p>
So far there are no peers apart from the web mirrors. Maybe we can spread the torrents across appropriate exits and seed from there? Write a script that easily allows anyone to “torrent mirror” Tor (plus explain how to easily www mirror the dist/ directory).
</p>

</div>

<h2 class="sectionedit8" id="website">Website</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> need help on writing press releases, proposals, funding applications etc., in all languages</div>
</li>
<li class="level1"><div class="li"> collect articles and howtos on tor for wiki</div>
</li>
<li class="level1"><div class="li"> design: other icons, onion graphic, logo? anyone?</div>
</li>
<li class="level1"><div class="li"> <a href="https://github.com/moba/torservers-website" class="urlextern" title="https://github.com/moba/torservers-website" rel="ugc nofollow">https://github.com/moba/torservers-website</a></div>
</li>
</ul>

</div>

<h3 class="sectionedit9" id="tech1">Tech</h3>
<div class="level3">
<ul>
<li class="level1"><div class="li"> enable website translations (move everything to dokuwiki? or use gettext on our simple website code?)</div>
</li>
<li class="level1"><div class="li"> contact form with gpg encryption</div>
</li>
<li class="level1"><div class="li"> move mailinglist; offer secure mailinglists. <a href="http://www.sympa.org/" class="urlextern" title="http://www.sympa.org/" rel="ugc nofollow">sympa</a> (used by riseup) or mailman?</div>
</li>
<li class="level1"><div class="li"> ticketing system for abuse</div>
</li>
<li class="level1"><div class="li"> bugtracker</div>
</li>
<li class="level1"><div class="li"> extra accounting for tor traffic  <a href="http://www.cyberciti.biz/faq/linux-configuring-ip-traffic-accounting/" class="urlextern" title="http://www.cyberciti.biz/faq/linux-configuring-ip-traffic-accounting/" rel="ugc nofollow">http://www.cyberciti.biz/faq/linux-configuring-ip-traffic-accounting/</a></div>
</li>
</ul>

</div>

<h2 class="sectionedit10" id="member_services">Member Services</h2>
<div class="level2">

<p>
I want to provide secure email accounts and similar services to members, other affiliated groups, social movements, or everyone. We can still decide on who to give them to, but in general I want to have that kind of stuff documented openly in our Wiki. I know it's not really “Torservers.net” related, but remember we're the people with all the abilities to help in other areas as well.
</p>

<p>
I played with <a href="/wiki/setup/mailserver" class="wikilink2" title="setup:mailserver" rel="nofollow" data-wiki-id="setup:mailserver">GPG encryption for incoming messages using Postfix</a>. How would you expand that to greater scale? Is “Sieve” the better mail filtering language? How to make things like that user configurable easily?
</p>

<p>
I am currently considering to install Tine2.0 on www as member service. It would require mysql and possibly openldap though. Comments? 
</p>

<p>
Other useful services might be Jabber, StatusNet, hidden services hosting etc.
</p>

<p>
Use extra bandwidth not used by Tor for
</p>
<ul>
<li class="level1"><div class="li"> Freenet</div>
</li>
<li class="level1"><div class="li"> I2P</div>
</li>
<li class="level1"><div class="li"> a Tor2web proxy</div>
</li>
</ul>

</div>

<h2 class="sectionedit11" id="other">Other</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> t-shirts</div>
</li>
<li class="level1"><div class="li"> <del>need a german document to proactively contact BKA, LKA and local Dresden police. please write one!</del> –&gt; Apparently they don't have a way of collecting and integrating such data into their work process, so this would seem rather pointless.</div>
</li>
</ul>

</div>

<h2 class="sectionedit12" id="organizations_to_contact_about_partnershipsponsoring">Organizations to contact about Partnership/Sponsoring</h2>
<div class="level2">

<p>
Add! Help us write contact letters. Locate possible funding opportunities, help us write applications.
</p>

</div>

<h3 class="sectionedit13" id="german">German</h3>
<div class="level3">
<ul>
<li class="level1"><div class="li">  <a href="http://www.stiftung-bridge.de/" class="urlextern" title="http://www.stiftung-bridge.de/" rel="ugc nofollow">Stiftung Bridge</a> - förderantrag bis april 2011 stellen</div>
</li>
<li class="level1 node"><div class="li">  Journalismus: <a href="http://linke-medienakademie.de/" class="urlextern" title="http://linke-medienakademie.de/" rel="ugc nofollow">Linke Medienakademie</a>, <a href="http://www.reporter-ohne-grenzen.de/" class="urlextern" title="http://www.reporter-ohne-grenzen.de/" rel="ugc nofollow">Reporter ohne Grenzen</a></div>
<ul>
<li class="level2"><div class="li">  Verlagshäuser: <a href="http://www.heise-medien.de/" class="urlextern" title="http://www.heise-medien.de/" rel="ugc nofollow">Heise</a>, <a href="http://www.spiegelgruppe.de/" class="urlextern" title="http://www.spiegelgruppe.de/" rel="ugc nofollow">Spiegel</a></div>
</li>
</ul>
</li>
<li class="level1"><div class="li">  Menschenrechte: <a href="http://www.transparency.de/" class="urlextern" title="http://www.transparency.de/" rel="ugc nofollow">Transparency International</a></div>
</li>
<li class="level1"><div class="li">  <a href="http://whistleblower-netzwerk.de/" class="urlextern" title="http://whistleblower-netzwerk.de/" rel="ugc nofollow">Whistleblower-Netzwerk</a></div>
</li>
<li class="level1"><div class="li">  IT: <a href="http://akzensur.de/" class="urlextern" title="http://akzensur.de/" rel="ugc nofollow">AK Zensur</a>, <a href="http://www.ccc.de/" class="urlextern" title="http://www.ccc.de/" rel="ugc nofollow">CCC</a>, <a href="http://www.privacyfoundation.de/" class="urlextern" title="http://www.privacyfoundation.de/" rel="ugc nofollow">GPF</a>, <a href="http://www.fiff.de" class="urlextern" title="http://www.fiff.de" rel="ugc nofollow">FIfF</a>, <a href="http://www.datenschutzverein.de" class="urlextern" title="http://www.datenschutzverein.de" rel="ugc nofollow">DVD</a>, <a href="https://mogis-verein.de/" class="urlextern" title="https://mogis-verein.de/" rel="ugc nofollow">Mogis</a>, <a href="http://www.gi-ev.de/" class="urlextern" title="http://www.gi-ev.de/" rel="ugc nofollow">Gesellschaft für Informatik</a>, <a href="http://www.foebud.org/" class="urlextern" title="http://www.foebud.org/" rel="ugc nofollow">FoeBuD</a>?</div>
</li>
<li class="level1"><div class="li"> <a href="http://www.bewegungsakademie.de/" class="urlextern" title="http://www.bewegungsakademie.de/" rel="ugc nofollow">http://www.bewegungsakademie.de/</a> , <a href="http://www.linke-journalisten.de/" class="urlextern" title="http://www.linke-journalisten.de/" rel="ugc nofollow">http://www.linke-journalisten.de/</a></div>
</li>
<li class="level1"><div class="li"> Sonstige: <a href="http://www.anwaltverein.de/" class="urlextern" title="http://www.anwaltverein.de/" rel="ugc nofollow">Deutscher Anwaltverein</a>, <a href="http://www.attac.de" class="urlextern" title="http://www.attac.de" rel="ugc nofollow">Attac</a></div>
</li>
</ul>

</div>

<h3 class="sectionedit14" id="nach_anerkennung_der_gemeinnuetzigkeit">Nach Anerkennung der Gemeinnützigkeit</h3>
<div class="level3">
<ul>
<li class="level1"><div class="li">  <a href="http://www.spendenportal.de/" class="urlextern" title="http://www.spendenportal.de/" rel="ugc nofollow">spendenportal.de</a></div>
</li>
</ul>

</div>

<h3 class="sectionedit15" id="english">English</h3>
<div class="level3">
<ul>
<li class="level1"><div class="li">  <a href="http://www.ifj.org/" class="urlextern" title="http://www.ifj.org/" rel="ugc nofollow">International Federation of Journalists</a>, <a href="http://www.projectcensored.org/" class="urlextern" title="http://www.projectcensored.org/" rel="ugc nofollow">Project Censored</a>, <a href="http://www.rfa.org/english" class="urlextern" title="http://www.rfa.org/english" rel="ugc nofollow">Radio Free Asia</a> / <a href="http://www.rfaunplugged.org/" class="urlextern" title="http://www.rfaunplugged.org/" rel="ugc nofollow">RFA Unplugged</a>, <a href="http://www.humanrightsfirst.org/" class="urlextern" title="http://www.humanrightsfirst.org/" rel="ugc nofollow">Human Rights First</a>, <a href="http://globalvoicesonline.org/" class="urlextern" title="http://globalvoicesonline.org/" rel="ugc nofollow">Global Voices</a>, <a href="http://www.indexoncensorship.org/" class="urlextern" title="http://www.indexoncensorship.org/" rel="ugc nofollow">Index on Censorship</a>, <a href="http://www.apc.org/" class="urlextern" title="http://www.apc.org/" rel="ugc nofollow">Association For Progressive Communications</a></div>
</li>
<li class="level1"><div class="li">  <a href="http://www.accessnow.org/" class="urlextern" title="http://www.accessnow.org/" rel="ugc nofollow">Access Now</a>, <a href="http://opennet.net/" class="urlextern" title="http://opennet.net/" rel="ugc nofollow">OpenNet Initiative</a>, <a href="http://www.hackersforcharity.org/" class="urlextern" title="http://www.hackersforcharity.org/" rel="ugc nofollow">Hackers for Charity</a>, <a href="http://www.eff.org/" class="urlextern" title="http://www.eff.org/" rel="ugc nofollow">EFF</a></div>
</li>
<li class="level1"><div class="li">  <a href="http://www.indymedia.org/" class="urlextern" title="http://www.indymedia.org/" rel="ugc nofollow">IndyMedia</a>, <a href="https://www.riseup.net" class="urlextern" title="https://www.riseup.net" rel="ugc nofollow">Riseup</a></div>
</li>
<li class="level1"><div class="li">  <a href="http://english.aljazeera.net/" class="urlextern" title="http://english.aljazeera.net/" rel="ugc nofollow">Al Jazeera</a> ?</div>
</li>
<li class="level1"><div class="li">  EDRI and members <a href="http://www.edri.org/about/members" class="urlextern" title="http://www.edri.org/about/members" rel="ugc nofollow">http://www.edri.org/about/members</a>