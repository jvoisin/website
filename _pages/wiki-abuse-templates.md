---
layout: page
title: Abuse & DMCA Templates
redirect_from: /wiki/abuse/templates/
permalink: /abuse-templates/
---
## ⚠️ This page is outdated ⚠️

A more recent version can be found here:<br> 
[https://community.torproject.org/relay/community-resources/tor-abuse-templates/](https://community.torproject.org/relay/community-resources/tor-abuse-templates/)

## Archived version

Let us work on good answer for repeating situations. Hopefully, this will help other node operators.

## Reply to Abuse (general)

    Hi $TO$,

    I am very sorry to hear that. The IP you quote hosts a Tor exit node
    (open relay). I can offer you to block specific destination IPs and 
    ports, but the abuser doesn't use our relays specifically; he/she 
    will just be routed through a different exit node outside of our 
    control instead.

    You will be much better off blocking Tor temporarily from your side.
    Please only issue a temporary block to not affect other, legitimate
    users of Tor.

    Tor is a research project, funded by the National Science Foundation and
    previously DARPA (among others). Its primary goal is to provide people
    from hostile environments with encrypted and uncensored access to the
    Internet. For more than a third of the worlds population, the Internet
    is being either filtered or monitored. Every day, activists and bloggers
    are imprisoned or threatened for what we in the western  countries see
    as a Human Right.

    There are usage stats on the www.torproject.org website that show that
    more than 500,000 users from China, Iran and similar regimes (have to)
    use Tor to access the Internet every day.

    Torservers.net is a non-profit organization comprised of volunteers who 
    are willing to run Tor relays for the benefit of everyone.

    I hope that you understand the importance of Tor, and don't block the 
    whole Tor network because of a single attack/misuse.

    Please let me know if I can be of further assistance. You can find a
    guide on how to identify all Tor exit nodes on the Torproject website
    www.torproject.org. Please send Tor users through additional "screening"
    (CAPTCHA, etc) instead of just blocking them completely. 

    Thanks for your understanding. If you have further questions, feel free
    to contact us again.

    Yours sincerely,
    -- 
    $FROM$
    Abuse Department 
    https://www.torservers.net/


## Abuse of Services

    We're sorry to hear about this incident. It is not our goal to support
    or condone criminal activities. We provide our services to people behind
    censoring firewalls in oppressive regimes. Tor is key technology for
    Internet users and citizens in China, Iran, Syria, Kazakhstan and many
    other countries to be able to access uncensored media and exchange
    information freely. We strongly stand for this and see it as a modern
    human right. Unfortunately, this means we have to take a close watch on
    illegal activities and abuse of anonymizing technology like Tor.

    Your reports are very important to understand how usage of Tor is
    developing over time. As a network of 13 non-profit organizations in 10
    countries, we push quite some large amounts of data at various locations
    for Tor (20G+). Based on the limited amount of information we are
    legally allowed to look at to protect our users, and the number of
    reports like yours, we still feel that the balance is very much on the
    legal side. We are in close contact with "regular" ISPs, and it seems
    our level of "abuse per traffic/number of users" is on par with what
    they see.

    Please understand that Tor makes it technically impossible to single out
    individual users. We also are legally bound to respective privacy
    rights. What I can offer is to block certain destination ports and IP
    addresses, but I strongly advocate against these types of blocks because
    they will affect _all_ users of Tor, not only the "bad apples". You can
    also simply block Tor users -- again, all of them! -- on your end. If
    you need help with that, let us know. If levels of abuse turn out to
    become too high, please consider to lift the block after some time so
    friendly users of Tor can again access your resources. If you value the
    privacy of your visitors, we can also talk about less "either-or"
    strategies modeled around your service.

    Thanks for your understanding! Please use $XYZ to best
    reach us should any issues arise.

    -- 
    Moritz Bartl 
    Abuse Department

## Blocking Request

    The attacker is using Tor. Tor will automatically select one of the
    hundreds of available exit servers, that's why you see changing IPs. We
    can block destination IPs for our small subset of Tor exit relays, but
    that won't really help, the attacker will not even notice the change in 
    exit relays available for him.

    The only real solution here is to take care of Tor users on your side.
    Please be aware that by blocking Tor, you also block all legitimate
    users! Please lift the ban after the attack is over, or take care of Tor
    users with other means (for example by only blocking access to sensitive
    content, displaying extra CAPTCHAs for Tor users, etc).

    You can detect Tor users via two mechanisms:

    https://check.torproject.org/cgi-bin/TorBulkExitList.py
    https://www.torproject.org/projects/tordnsel.html.en

    Please only issue a temporary ban so legitimate Tor users are not 
    affected for too long.

    Hope this helps!

## Email Spam

    Hi $TO$,

    Thanks for your report. The request passed through one of our Tor exit
    nodes. We do not allow email to be sent from our systems (port 25 is 
    blocked), so the offender must have used a web based email account. 
    You should direct your complaint to the mail server given in the email 
    header, so they can close and/or track down the account.

    Please let me know if I can be of further assistance.

    I know this very unfortunate. Thanks for your understanding!
    Yours sincerely,
    -- 
    $FROM$
    Abuse Department 
    http://www.torservers.net/ 

## DMCA Complaints

Unfortunately, the Tor network is sometimes being abused for file sharing. This hurts the network, and <a href="https://blog.torproject.org/blog/bittorrent-over-tor-isnt-good-idea" class="urlextern" title="https://blog.torproject.org/blog/bittorrent-over-tor-isnt-good-idea" rel="ugc nofollow">Bittorrent over Tor isn't a good idea</a> anyway.


If you run a Tor relay that allows arbitrary ports to exit, you sadly cannot stop this from happening. Many ISPs and data centers are scared over receiving automated notices by Bittorrent scanners like those run by <a href="https://en.wikipedia.org/wiki/MediaSentry" class="interwiki iw_wp" title="https://en.wikipedia.org/wiki/MediaSentry">MediaSentry</a> (now Peer Media Technologies). Because anyone can file a DMCA complaint and send such notices, this leads to the absurd situation that you can force the takedown of almost any server, even outside US jurisdiction - and even though a Tor operator is perfectly protected thanks to <a href="http://www.copyright.gov/title17/92chap5.html#512" class="urlextern" title="http://www.copyright.gov/title17/92chap5.html#512" rel="ugc nofollow">DMCA §512(a)</a> (and similar laws outside of the USA).

So far, we were out of luck about contacting MediaSentry about their reports. We reply to every single one of them, and we tried to get in contact with them over basically any email address we could find. For some funny reason, Peer Media Technologies still uses the name “MediaSentry” and the MX 

### Example Full Report


    Return-Path: &lt;CBSCopyright@mc.mediasentry.com&gt;
    Received: from relay01.mediasentry.com (relay01.mediasentry.com [66.192.117.41])
    	by xxxxx (Postfix) with ESMTP id 1143813EC138
    	for &lt;abuse@xxxxx&gt;; Tue, 19 Apr 2011 04:11:41 +0200 (CEST)
    Received: from KET086 (CASEAGENT [74.203.235.115])
	by relay01.mediasentry.com (Mailer) with ESMTP id 8BE5C3D1735
	for &lt;abuse@xxxxx&gt;; Mon, 18 Apr 2011 22:11:38 -0400 (EDT)
    From: MediaSentry Copyright Infringement &lt;CBSCopyright@mc.mediasentry.com&gt;
    Subject: Case ID 13322xxxxx - Notice of Claimed Infringement
    MIME-Version: 1.0
    Content-Type: multipart/mixed; Boundary="--PTCP_0000528202563b07db"
    Message-ID: &lt;0000528302563c07db@[74.203.235.115]&gt;
    Date: Mon, 18 Apr 2011 22:04:41 -0400



    ﻿Monday, April 18, 2011


    RE:  Unauthorized Distribution of the Copyrighted Television Series Entitled
       Navy: NCIS

    Dear xxxxxx:

    We are writing this letter on behalf of the relevant subsidiaries of CBS Corporation.

    We have received information that an individual has utilized the below-referenced IP address at the noted date and time to offer downloads of copyrighted television programs through a "peer-to-peer" service, including such title(s) as:

	Navy: NCIS

    The distribution of unauthorized copies of copyrighted television programs constitutes copyright infringement under the Copyright Act, Title 17 United States Code Section 106(3). This conduct may also violate the laws of other countries, international law, and/or treaty obligations.

    Since you own this IP address (77.247.181.163), we request that you immediately do the following:

    1) Remove or disable access to the individual who has engaged in the conduct described above; and

    2) Take appropriate action against the account holder under your Abuse Policy/Terms of Service Agreement.

    We also would request that you inform the individual who engaged in this conduct that legitimate copies of CBS content are widely available for viewing online, for example on www.cbs.com and many other sites that participate in the CBS Audience Network. 

    On behalf of CBS, owner of the exclusive rights in the copyrighted material at issue in this notice, we hereby state that we have a good faith belief that use of the material in the manner complained of is not authorized by CBS, its respective agents, or the law.

    Also, we hereby state, under penalty of perjury, that the information in this notification is accurate and that we are authorized to act on behalf of the owner of the exclusive rights being infringed as set forth in this notification.

    Please direct any end user queries to the following: 

    CopyrightQs@mediasentry.com


    Please include the Case ID 13322xxxxx, also noted above, in the subject line of all future correspondence regarding this matter.


    We appreciate your assistance and thank you for your cooperation in this matter. Your prompt response is requested.

    Respectfully,

    D. Brewer
    Enforcement Coordinator
    Peer Media Technologies, Inc.
    copyrightqs@mediasentry.com

    ------------------------------
    INFRINGEMENT DETAIL
    --------------------

    Infringing Work: Navy: NCIS
    First Found: 18 Apr 2011 20:26:43 EDT (GMT -0400)
    Last Found: 18 Apr 2011 20:26:43 EDT (GMT -0400)
    IP Address: 77.247.181.163
    IP Port: 6881
    Protocol: BitTorrent
    Torrent InfoHash: 62E893EBC75376FEA8C5B19141F7D90019CB5CA6
    Containing file(s):
    NCIS.S08E17.HDTV.XviD-LOL.[VTV].avi.torrent (366,360,300 bytes)


### Reply

    Dear Peer Media Technologies, 

    We do not keep customer usage information. As a German non-profit
    organization, we are bound to the German Telemediengesetz
    ("telemedia law") which prohibits logging of customer usage data unless
    required for billing purposes (TMG section 15).

    Our systems do not host content and fall under section 512 of the DMCA
    "Transitory Digital Network Communications" and similar laws. We do
    not control or select the transmission, routing or provision of 
    connections.

    We are not interested in supporting criminal activity. To the contrary,
    our networks are being used to run Tor exits. Tor is a research project,
    funded by the National Science Foundation and previously DARPA (among
    others). Its primary goal is to provide people from hostile environments
    with encrypted and uncensored access to the Internet. For more than a
    third of the worlds population, the Internet is being either filtered or
    monitored. Every day, activists and bloggers are imprisoned or
    threatened for what we in the western countries see as a Human Right.

    There are usage stats on the www.torproject.org website that show that
    more than 500,000 users from China, Iran and similar regimes use Tor to
    access the Internet every day.

    At www.torservers.net, we offer to run Tor nodes for people and
    organizations willing to help monetarily.

    You can find more information specifically about abuse scenarios and the
    legal situation at http://www.torservers.net/abuse.html

    Thanks for your understanding. If you have further questions, feel free
    to contact us again.

### Procmail Rules

    :0 c
    * ^To:.*abuse@example.com
    * ^From:.*(mediasentry.com|copyright-compliance.com|copyright-notice.com|ip-echelon.com)
    {
      REPLYTO = `formail -xFrom:`
      SUBJECT = `formail -xSubject:`

      :0 fhb
      | (/usr/bin/formail -r                    \
       -I"To: $REPLYTO"     \
       -I"From: Torservers Abuse Team &lt;abuse@torservers.net&gt;" \
       -I"Subject: Re: $SUBJECT" ;    \
       /bin/cat /home/user/dmca-autoreply.msg) \
      | /usr/sbin/sendmail -oi -t
    }

    :0
    * ^To:.*abuse@example.com
    * ^From:.*(mediasentry.com|copyright-compliance.com|copyright-notice.com|ip-echelon.com)
    .Torservers.Abuse.DMCA/</pre>
