---
layout: page
title: Support us
redirect_from: /donate.html
permalink: /support-us/
---
## Donate

Torservers.net is a network of non-profit organizations. It is very important for the Tor network to be supported and operated by a diverse set of players, so please [pick a partner and donate to them](/partners) directly.
	
If you don't want to pick, just select one at random! We're all great and appreciate your support! 🙏

You can even go a stepp further and found a relay-operators-organisation.

## As an ISP

You want to support Tor, and have spare server and bandwidth resources? You are able to provide IPs for bridges? [Contact us](/contact)! Our [partners](/partners) will maintain the nodes for you. Even the smallest amount of bandwidth helps!
