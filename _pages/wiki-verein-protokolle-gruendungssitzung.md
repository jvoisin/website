---
layout: page
title: Gründungssitzung 26.03.2011
permalink: /wiki/verein/protokolle/gruendungssitzung/
---
<h2 class="sectionedit2" id="tagesordnung">Tagesordnung</h2>
<div class="level2">
<ol>
<li class="level1"><div class="li"> Eröffnung der Versammlung und Begrüßung</div>
</li>
<li class="level1"><div class="li"> Bestellung eines Protokollführers</div>
</li>
<li class="level1"><div class="li"> Anerkennung der Satzung</div>
</li>
<li class="level1"><div class="li"> Wahl des Vorstands</div>
</li>
<li class="level1"><div class="li"> Wahl der Finanzprüfer</div>
</li>
<li class="level1"><div class="li"> Festlegung der Mitgliedsbeiträge</div>
</li>
</ol>

</div>

<h2 class="sectionedit3" id="begruessung">Begrüßung</h2>
<div class="level2">

<p>
Es trafen sich die in Anlage 1 aufgeführten Personen, um den Förderverein Zwiebelfreunde zu gründen. Moritz Bartl begrüßte die Teilnehmer und eröffnete die Versammlung. 
</p>

</div>

<h2 class="sectionedit4" id="wahl_des_versammlungsleiters">Wahl des Versammlungsleiters</h2>
<div class="level2">

<p>
Auf Vorschlag der Versammlung wurde Moritz Bartl zum Versammlungsleiter gewählt. Er nahm die Wahl an. 
</p>

</div>

<h2 class="sectionedit5" id="wahl_des_protokollfuehrers">Wahl des Protokollführers</h2>
<div class="level2">

<p>
Auf Vorschlag der Versammlung wurde Julian Wissmann als Protokollführer vorgeschlagen und gewählt. Er nahm die Wahl an. 
</p>

</div>

<h2 class="sectionedit6" id="beschlussfassung_der_tagesordnung">Beschlussfassung der Tagesordnung</h2>
<div class="level2">

<p>
Der Versammlungsleiter schlug die Tagesordnung vor, die sich aus der diesem Protokoll als Anlage 2 beiliegenden Einladung ergibt. Diese wurde von der Versammlung ohne Veränderungen beschlossen. 
</p>

</div>

<h2 class="sectionedit7" id="beschlussfassung_ueber_die_vereinssatzung">Beschlussfassung über die Vereinssatzung</h2>
<div class="level2">

<p>
Die Anwesenden erörterten den Satzungsentwurf. Dann stimmten alle Anwesenden über die in diesem Protokoll als Anlage 3 beiliegenden Fassung der Satzung durch Handzeichen ab. Der Vereinsleiter stellte die Gründung des Vereins fest. Daraufhin unterzeichneten alle Gründungsmitglieder die Satzung und erklärten damit den Beitritt zum Verein. 
</p>

</div>

<h2 class="sectionedit8" id="wahl_des_vorstands">Wahl des Vorstands</h2>
<div class="level2">

<p>
Es wurde um Vorschläge für die Vorstandsmitglieder gebeten. Folgende Mitglieder wurden vorgeschlagen und in geheimer Abstimmung gewählt: 
Vorsitzender: Moritz Bartl           Schatzmeister: Juris Vetra           Schriftführer: Julian Wissmann
</p>

<p>
Der Vorsitzende wurde mit 13 Ja-Stimmen, 0 Nein-Stimmen und 0 Enthaltungen gewählt. Der Schatzmeister wurde mit 11 Ja-Stimmen,   0 Nein-Stimmen, 1  Enthaltungen und einer ungültigen Stimme gewählt. Der Schriftführer wurde mit 12 Ja-Stimmen, 0 Nein-Stimmen und 1 Enthaltungen gewählt. 
Sie nahmen die Wahl an.
</p>

</div>

<h2 class="sectionedit9" id="beschlussfassung_ueber_die_mitgliedsbeitraege">Beschlussfassung über die Mitgliedsbeiträge</h2>
<div class="level2">

<p>
Es wurden folgende Beiträge beschlossen: Regulärer Beitrag: 60 Euro Ermäßigter Beitrag: 25 Euro. 
</p>

</div>

<h2 class="sectionedit10" id="sonstiges">Sonstiges</h2>
<div class="level2">

<p>
Der Vorstand wurde beauftragt, die Anmeldung beim Vereinsregister vorzunehmen sowie die Gemeinnützigkeit beim Finanzamt zu beantragen. Die Versammlung wurde um 16 Uhr geschlossen. 
</p>

<p>
<br>

<br>

Unterschrift Versammlungsleiter  / Unterschrift 1. Vorsitzender  / Unterschrift Protokollführer 
</p>