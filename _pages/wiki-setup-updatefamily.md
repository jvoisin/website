---
layout: page
title: Update MyFamily
permalink: /wiki/setup/updatefamily/
---
## ⚠️ This page is outdated ⚠️

<a href="https://github.com/torservers/myfamilyupdater" class="urlextern" title="https://github.com/torservers/myfamilyupdater" rel="ugc nofollow">https://github.com/torservers/myfamilyupdater</a>
<pre class="code">sed -i -e "s:^MyFamily.*:`wget -O - https://www.torservers.net/misc/config/torrc | grep MyFamily`:g" /etc/tor/tor*</pre>
Todo: Add missing signature check!
