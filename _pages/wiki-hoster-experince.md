---
layout: page
title: Hosters experience
permalink: /wiki/hoster/experience/
---
We are running and did run Tor exit nodes at different Providers and Datacenters. This page documents our experience with them.

<h1 class="sectionedit1" id="axigy_us">Axigy (US)</h1>
<div class="level1">

<p>
<a href="http://www.axigy.com/" class="urlextern" title="http://www.axigy.com/" rel="ugc nofollow">http://www.axigy.com/</a>
</p>
<pre class="code">E3-1230v2
8 GB DDR3 RAM
500 GB SATA
Full Gbit/s
6 IP Addresses with 24/7 KVM</pre>
<ul>
<li class="level1"><div class="li"> 03.04.2011: very friendly and understanding, ordered (“Great, I like what you're doing to help those countries with respective governments. In response to Abuse, we are very understanding, complaints are actually forwarded automatically in the event we receive any from your range.”)</div>
</li>
<li class="level1"><div class="li"> 29.04.2011: CEO contacted me. Datacenter is being closed, very friendly, offered to move us to different DC or help us find another ISP. Chatted a while about stuff.</div>
</li>
<li class="level1"><div class="li"> 07.05.2011: new server, better specs, a bit more expensive ($210 vs. $199 before)</div>
</li>
<li class="level1"><div class="li"> 08.09.2011: new server in new datacenter; 50TB each direction</div>
</li>
<li class="level1"><div class="li"> 05.11.2011: new deal: full gbit for $310 monthly</div>
</li>
<li class="level1"><div class="li"> 07.08.2012: 2x gbit for $800</div>
</li>
</ul>

</div>

<h1 class="sectionedit2" id="nl">NL</h1>
<div class="level1">
<pre class="file">&gt; I can do the following:
&gt; HP DL120 G6
&gt; Processor(s) Intel 4-core 2.4 GHz (X3430)
&gt; Memory 8 GB
&gt; Harddisk(s) 2x 250 GB
&gt; Uplink port 1 GBIT
&gt; Incoming bandwidth Unmetered
&gt; Outgoing bandwidth 100 TB
&gt; APC control Yes
&gt; KVM over IP Yes
&gt; Quarterly: **** euro excl 19% VAT --&gt; **** euro incl 19% VAT
&gt; I will copy the same config for free for you as sponsership.</pre>
<ul>
<li class="level1"><div class="li"> 02.04.2011: asked about Tor hosting and possible sponsorship</div>
</li>
<li class="level1"><div class="li"> 02.04.2011: ISP asked for details about abuse etc</div>
</li>
<li class="level1"><div class="li"> 04.04.2011: ISP confirmed possible sponsorship, donates a second box</div>
</li>
<li class="level1"><div class="li"> 07.04.2011: RIPE WHOIS reassigned, online!</div>
</li>
<li class="level1"><div class="li"> 27.08.2011: price lowered per quarter</div>
</li>
</ul>

</div>

<h1 class="sectionedit3" id="euserv_de">EUserv (DE)</h1>
<div class="level1">

<p>
<a href="https://ssl.euserv.de/produkte/specials/spring-server2008.php" class="urlextern" title="https://ssl.euserv.de/produkte/specials/spring-server2008.php" rel="ugc nofollow">https://ssl.euserv.de/produkte/specials/spring-server2008.php</a>
</p>
<pre class="file">Intel(R) Pentium(R) Dual  CPU  E2180  @ 2.00GHz
12GB RAM
2x500GB
100 mbit/s
540€ per year (45 per month)</pre>
<ul>
<li class="level1"><div class="li"> 04.04.2011: Old server signed over to us by another Tor community member. Thanks!</div>
</li>
</ul>

</div>

<h1 class="sectionedit4" id="voxility_ro">Voxility (RO)</h1>
<div class="level1">

<p>
<a href="http://www.limehost.ro/servere/dedicated-models.html" class="urlextern" title="http://www.limehost.ro/servere/dedicated-models.html" rel="ugc nofollow">http://www.limehost.ro/servere/dedicated-models.html</a>
</p>
<pre class="file">	 
Core i7-2600 @ 3.40 GHz	 
8 GB DDR3-1333	 
2 HDD x 1 TB SATA3	 
1 Gbit/s	 
50€ Setup	 
103€ monthly</pre>
<ul>
<li class="level1"><div class="li"> 06.01.2012: established contact with limehost.ro, same day reply from voxility and agreed to host Tor exits and reassign IPs	 </div>
</li>
<li class="level3"><div class="li"> 25.01.2012: ordered server	 </div>
</li>
<li class="level3"><div class="li"> 26.01.2012: email, order will be delayed due to RIPE registration	 </div>
</li>
<li class="level3"><div class="li"> 01.02.2012: server went online</div>